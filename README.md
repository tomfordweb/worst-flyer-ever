.
1) Create a basic job that will echo something to `public/index.html`
2) Show where to access artifacts in builds
3) Create a `public/index.html`, add some content to it that will allow for string replacement.
4) Create a new pages stage, push project, see it deploy.
5) Use wget to download an image, in the pipeline, add it to public path
```
wget --output-document=public/image.jpg http://some-url
```
6) Move this to a variable
7) deploy completed job
8) Optimize file
